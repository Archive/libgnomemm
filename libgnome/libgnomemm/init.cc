// -*- c++ -*-
/* $Id$ */

/* init.cc
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomemm/init.h>
#include <gtkmm/main.h> //For Gtk::Main::init_gtkmm_internals()
#include <libgnomemmconfig.h> //For LIBGNOMEMM_VERSION
#include <libgnomemm/wrap_init.h>
#include <libgnome/gnome-init.h> //For libgnome_module_info_get()

namespace Gnome
{

static void
libgnomemm_post_args_parse(GnomeProgram *program, GnomeModuleInfo* mod_info)
{
  Gtk::Main::init_gtkmm_internals();
  wrap_init();
}

ModuleInfo& module_info_get()
{
  static Gnome::ModuleInfo info("libgnomemm", LIBGNOMEMM_VERSION, "C++ wrappers for libgnome.");

  //Requirements:
  static GnomeModuleRequirement req[3];

  req[0].required_version = "1.102.0";
  req[0].module_info = libgnome_module_info_get(); //The same as LIBGNOME_MODULE

  req[1].required_version = LIBGNOMEMM_VERSION;
  req[1].module_info = module_info_get_cpp_only().gobj(); //Does wrap_init()

  req[2].required_version = NULL;
  req[2].module_info = NULL;

  info.set_requirements(req);

  return info;
}

ModuleInfo& module_info_get_cpp_only()
{
  static Gnome::ModuleInfo info_cpp("libgnomemm_cpp_only", LIBGNOMEMM_VERSION, "C++ wrappers for libgnome - C++ part only.");

  info_cpp.set_post_args_parse(&libgnomemm_post_args_parse);

  return info_cpp;
}

} //namespace Gnome
