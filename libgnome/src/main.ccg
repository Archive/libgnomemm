/* $Id: main.ccg,v 1.11 2006/04/03 06:17:28 murrayc Exp $ */
// -*- C++ -*- // this is for the .ccg, I realize gensig puts one in

/* main.cc
 * 
 * Copyright (C) 1998 EMC Capital Management Inc.
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * Copyright (C) 1999 The Gtk-- Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnome/libgnome.h>

namespace Gnome
{

char* Main::app_id_ = 0;
char* Main::app_version_ = 0;


Main::Main(const Glib::ustring& app_id, const Glib::ustring& app_version,
           const ModuleInfo& module_info,
           int argc, char** argv)

{
  set_ids(app_id, app_version);

  GnomeProgram* pProgram = gnome_program_init(app_id_, app_version_, module_info.gobj(), argc, argv, 0);
  m_refProgram = Glib::wrap(pProgram, false);
}


Main::Main(const Glib::ustring& app_id, const Glib::ustring& app_version,
           const ModuleInfo& module_info,
           int argc, char** argv,
           const struct poptOption* options, int flags, poptContext* return_ctx)
{
  set_ids(app_id, app_version);

  //This code is copied from the libgnomeui2 implementation of the deprecated gnome_init_with_popt_table() function:
  GnomeProgram* pProgram  = gnome_program_init(app_id_, app_version_,
            module_info.gobj(),
            argc, argv,
            GNOME_PARAM_POPT_TABLE, options,
            GNOME_PARAM_POPT_FLAGS, flags,
            (char*)0);
  m_refProgram = Glib::wrap(pProgram, false);

  if(return_ctx)
  {
    GValue value = { 0, };

    g_value_init (&value, G_TYPE_POINTER);
    g_object_get_property (G_OBJECT (pProgram),
                           GNOME_PARAM_POPT_CONTEXT, &value);
    *return_ctx = (poptContext)g_value_peek_pointer (&value);
    g_value_unset (&value);
  }
}


Main::Main(const Glib::ustring& app_id, const Glib::ustring& app_version,
           const ModuleInfo& module_info,
           int argc, char** argv,
           Glib::OptionContext& options)
{
  set_ids(app_id, app_version);

  GnomeProgram* pProgram  = gnome_program_init(app_id_, app_version_,
            module_info.gobj(),
            argc, argv,
            GNOME_PARAM_GOPTION_CONTEXT, options.gobj(),
            (char*)0);
  m_refProgram = Glib::wrap(pProgram, false);
}


Main::~Main()
{
  g_free(app_id_);
  g_free(app_version_);
}

void Main::set_ids(const Glib::ustring& app_id, const Glib::ustring& app_version)
{
  app_id_ = g_strdup(app_id.c_str());
  app_version_ = g_strdup(app_version.c_str());
}

} /* namespace Gnome */

